# Guía interactiva de MATLAB
Para usar esta guía, descarga todos los archivos [haciendo click aquí](https://gitlab.com/theryancasas/guia-interactiva-matlab/-/archive/master/guia-interactiva-matlab-master.zip) o clonándolo con `git` y abre el archivo `interactiveGuide.mlx` que encontrarás en la raíz, ya sea con [MATLAB online](https://matlab.mathworks.com) o con una versión de MATLAB igual o superior a la de 2016.
Se recomienda siempre la última versión, pues se incluyen controles interactivos con los que poder modificar partes del código de forma gráfica.

# Carpeta `extras`
Dentro de la carpeta de extras hay algunos apuntes más que surgen de las inquietudes de estar jugando con MATLAB y querer replicar los conocimientos adquiridos en la parte teórica de la asignatura, como por ejemplo, el cómo hacer un ajuste que no sea polinómico, explicado en `customFit.mlx`.

# Licencia de la guía
Esta guía y repositorio se rige bajo la licencia GPLv3. Lee el archivo LICENSE para más información.