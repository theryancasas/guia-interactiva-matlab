% Sumatorio desde 1 hasta final de i
function numero = contadorMagico(final)
    numero = 0;
    for i = 1:final
        numero = numero + i;
    end
end