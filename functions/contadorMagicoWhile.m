% Sumatorio desde 1 hasta final de i (implementado con un bucle while)
function numero = contadorMagicoWhile(final)
    numero = 0;
    i = final;
    
    while i ~= 0        % ~= es el != de MATLAB
        numero = numero + i;
        i = i - 1;
    end
end