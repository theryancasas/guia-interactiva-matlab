% Determinamos si un número es o no par
function phrase = esPar(number)
    phrase = number + " es un número ";
    if mod(number,2) == 0 
        phrase = phrase + "par";
    else
        phrase = phrase + "impar";
    end
    phrase = phrase + ".";
end