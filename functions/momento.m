%Calcula el momento N de una sola variable
function value = momento(x, N)  % La cabecera, con los parámentros de entrada y el nombre
    value = sum(x.*N) / sum(N); % El cuerpo de la función
end                             % el delimitador de final de una función

% La función debe estar definida en un archivo que lleve el mismo nombre.
% En este caso, la función se llama momento, y devuelve un valor en una
% variable "value".